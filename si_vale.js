const util = require('util'),
    rp = require('request-promise'),
    setIntervalPromise = util.promisify(setInterval),
    notifier = require('node-notifier');

let currencyRequestor = async () => JSON.parse(await rp.get(`https://sivale.tucuponfacil.com/api/customers/campaigns/6066d1a27f776`));

(async () => {



    setIntervalPromise(async () => {

        let items = await currencyRequestor(),
            filtered = items,
            now = new Date();

        filtered.promos = filtered.promos.filter(item => item.name === 'Amazon' || item.name === 'Spotif')
        // filtered.promos = filtered.promos.filter(item => item.name === 'Amazon')

        filtered.promos = filtered.promos.map((i) => ({

            name: i.name,
            description: i.description,
            no_codes: i.no_codes,
            status: i.status,
            created_at: i.created_at,
            updated_at: i.updated_at,
            message: i.message,
            viewers: i.viewers,
            max_exchangeable_codes: i.max_exchangeable_codes,
            available: i.available,
            redimidos: i.redimidos
        }));

        console.log(`${now.getHours()}:${now.getMinutes()}`);
        console.log(filtered);

        let checkPrice = (i) => i.available || i.redimidos;

        if (filtered.promos.some((i) => checkPrice(i))) {
        	console.log('========================================= Price !!! =========================================');
		console.log('Remember, your number is: 1993120022033020');
        	for (item of filtered.promos) {
        		if (checkPrice(item)) {
        			console.log(item);
        			notifier.notify(item.name);
        		}
        	}
        }
    }, 5000);

})();
